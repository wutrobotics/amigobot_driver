AmigoBot SH-based Control & Operations Software (AmigoSH)
Copyright (c) 2004-2005 ActivMedia Robotics, LLC.
Copyright (c) 2006, 2007 MobileRobots Inc.
All rights reserved.
=======================================================================

AmigoSH is the embedded software that runs on your AmigoBot's 
Renesas SH2-based controller. AmigoBot's controller also contains
Maintenance Mode utilities (Amigostub GDB interface) and operating
parameters in its FLASH memory.

Use the AmigoSHcf tool to update your AmigoSH software and to manage
your AmigoBot's operating parameters. Consult your robot's Operations
Manual for details.

        =============IMPORTANT=============
Use ONLY the AmigoSHcf configuration program that comes with this
distribution to manage your AmigoSH updates and robot FLASH parameters.
        ===================================

What's New in Version 2.1 and 2.2?
==================================
- Calibration routine
- Fixed windup and stutter when max vels not achievable
- Fixed reconnection odometry error
- Cf program updates

===========================
Calibration Routine
===========================
Three parameters intimately affect the performance of your mobile robot:
driftFactor, revCount and ticksMM. Two more if you have a gyro:
gyroCW and gyroCCW. Each of these are settable empirically to match your
individual robot and operating conditions, usually by running
the ARIA demo and measuring actual versus reported parametrics. With
the new firmware, a calibration tool is embedded in the firmware and accessible
through LCD Interactive mode to help you measure and compute these
operations parameters.

Please read the robot's documentation about the parameters, and follow the
LCD's onscreen instructions. Note that you must SAVE the derived values
to FLASH with the cf program in order to have them take effect.

==========================
Fixed Windup and Stutter
==========================
If your transVelMax and/or rotVelMax values are set above what your robot
can actually achieve, you may have noticed the platform stutter after driving
a long distance or rotating several revolutions. In extreme cases, the robot
might actually reverse direction for a short time. Also, after a long drive,
the robot might take much longer than expected to slow down and stop.

These behaviors happened because the ideal versus actual setpoints for
operation grew wildly apart, leading to very large windups and value over-
runs in the trajectory controller. These conditions are now recognized and
ameliorated in the firmware.
  
===========================
Reconnection Odometry Error
===========================
If you stop and restart the client-server connection quickly, such as in
the case when the client does not cleanly shut down the server before
disconnecting and then makes a reconnection, the server did not have time
to properly re-initialize its systems, particularly its odometry estimates,
leading to a "jump" in the odometry moments after reconnection. Now, the
server disallows reconnection until after it runs through its close-server
initialization loop.

==================
Cf Program Updates
==================
We've made several improvements to the configuration program. First,
it consults /etc/Aria.args (simple text file listing Aria startup arguments)
for the serial connection port. The -rp startup arguments still works, of
course, and takes precedence.

Now, too, the program warns you if you attempt to manage a later version of
the firmware. Firmware uploads by an older configuration program are okay,
but you could lose FLASH parameters.

Finally, you cannot use a configurator from a different platform; uARCScf
to upload/manage an ARCOS-based Pioneer 3-DX, for example.

=====================================
AmigoSH distributions for Release 2.2
=====================================
Linux:
------
Normally located in /usr/local,'tar -zxvf AmigoSH2_2.tgz' creates:
  AmigoSH/
	README (me)
	AmigoSH2_2.mot (AmigoSH image)
	AmigoSHcf (Download and configuration tool)
	amigobot.rop (default FLASH parameters for the AmigoBot)

Windows
-------
Double-click or otherwise execute the self-extracting archive AmigoSH2_2.exe.
Creates:

   C:\Program Files\MobileRobots\AmigoSH\
	README (me)
	AmigoSH2_2.mot (AmigoSH image)
	AmigoSHcf.exe (Download and configuration tool)
	amigobot.rop (default FLASH parameters for AmigoBot)

=========================
Operating AmigoSHcf
=========================
Like previous robot configuration tools, AmigoSHcf runs on a Linux
or Windows PC and communicates with the robot controller through a serial
connection. Accordingly, use a "pass-through" serial cable to connect your
PC's serial port -- COM1 or /dev/ttyS0, by default -- to your AmigoBot's
HOST serial port just inside the access port on the top of the robot.

AmigoSHcf Startup Arguments
----------------------------- 
*   -h                ; help message and exit
*   -rp [serial dev]  ; specify comm port if other than the default COM1 or /dev/ttyS0
*   -rb [baudrate]    ; specify initial HOST baudrate if other than the
			default 9600. AmigoSHcf eventually autobauds if you get
			the rate wrong.
*   -n                ; don't connect with controller
*   -u <path/motfile> ; upload AmigoSH motfile image to controller
			must be an AmigoSHV_v.mot file
*   -l <ropfile>      ; load a Robot Operating Parameters (.rop) file
*   -s <ropfile>      ; saves the ROP to disk on exit
*   -b <options>      ; execute interactive command options, then exit
*   -r <ropfile>      ; restore params from file, uploads if batch mode

For example, to upload the new AmigoSH image through your PC's serial port
COM3 (/dev/ttyS2):

(Linux)
$ cd /usr/local/AmigoSH2_2
$ ./AmigoSHcf -rp /dev/ttyS2 -u AmigoSH2_2.mot

(Windows) Launch a DOS-like command window (cmd.exe), then
>cd C:\Program Files\MobileRobots\AmigoSH
>AmigoSHcf -rp COM3 -u AmigoSH2_2.mot

Once connected, AmigoSH lets you interactively manage FLASH and related files.

AmigoSH Interaction Options
--------------------------
* '?' or 'h' or 'help' to see menu again
* 'v' or 'view' for current parameters
* 'r [ropfile]' or 'restore [ropfile]' to restore FLASH or [ropfile] rop values
* 's [ropfile]' or 'save [ropfile]' to save changes to FLASH or to disk if [ropfile]
* 'u <motfile>' or 'upload <motfile>' to upload new AmigoSH image (.mot file required)
* 'q' or 'quit' to exit

Contact http://robots.mobilerobots.com/techsupport with questions, concerns
or comments.

=======HISTORY=======
v2.2  05-21-07  Refinements to trajectory overruns
		Fixed no-argument single packet requests

v2.1  04-03-07  Added Calibration utility to LCD interactive mode
		Fixed trajectory controller windup and over-runs
		Fixed reconnection odometry error
		Updates to AmigoSHcf

v2.0  01-24-07  25ms re-issue of HEAD if HasGyro 2
		Fixed decel distance computation with gyro
		Got rid of rotation recoil with gyro
		Gyro correction in firmware
		Sync'd position integration with Std SIP timing
		Fixed rotVel for sipcycle vagaries
		Removed stalls triggered by maximizing PWM
		Max error now maxPIDout/Kp..Kd; removes large windup
		WatchDog timeout audible warning
		Added test in AmigoSHcf for version control

v1.3  04-25-06	Fixed driftCompensator reverse-drive code
		View and change selected ports with LCD's toggles
	        LCD monitor restores if signal or power distrupted
		rotVel int at end of std SIP
		AmigoSHcf now supports '-r <ropfile>' startup argument
		faultFlags int at end of SIP after rotVel

v1.2  01-24-06  Fixed sonar max range value
		Update LCD start up message

v1.1  05-10-05  Fixed sonar errors
		Embedded version major/minor ascii numbers @ 0x8400,1
		No joystick or joystick packets

v1.0  04-29-05  Initial production release of AmigoSH.
