# Amigobot hardware

For now, Amigobot robots in our lab don't have onboard linux, so there was a need for stable, full-duplex and wireless communication with PC. The choice was a bluetooth standard with HC-05v2 module with translator from UART to RS232, as shown below:

![Amigobot.png](https://bitbucket.org/repo/n8GLGo/images/3885297836-Amigobot.png)

Detailed information about used hardware:

* [RS232-UART Converter](http://botland.com.pl/konwertery-usb-uart-rs232-rs485/4475-konwerter-rs232-uart-ze-zlaczem-db9-33v5v.html)
* [HC-05v2 Bluetooth module](http://botland.com.pl/moduly-bluetooth/2891-modul-bluetooth-hc-05-v2.html)

# Compilation 

    $ nawigate to your ros workspace/src
    $ git clone https://github.com/amor-ros-pkg/rosaria.git
    $ git clone https://user_name@bitbucket.org/wutrobotics/amigobot_driver.git
    $ cd ..
    $ catkin_make

# Bluetooth set-up

To successfully connect to the robot you'll need bluetooth device in your computer, for example something like [this](http://botland.com.pl/moduly-bluetooth/4291-modul-bluetooth-20-usb-quer-kom0637.html) (if you don't have a built in one). Then turn the robot on, and open ubuntu bluetooth panel:

![1.png](https://bitbucket.org/repo/n8GLGo/images/1802646183-1.png)

Click the '+' icon to add new device and search for AmigoBot1 or AmigoBot2:

![2.png](https://bitbucket.org/repo/n8GLGo/images/2645767680-2.png)

Click pin options, and choose 1234:

![3.png](https://bitbucket.org/repo/n8GLGo/images/325027114-3.png)

After all of this you should see something similar to this:

![4.png](https://bitbucket.org/repo/n8GLGo/images/1802548309-4.png)

Then you'll have to copy rfcomm.conf to /etc/bluetooth directory.

    $ navigate to your ros workspace/src/amigobot_driver/config
    $ sudo cp rfcomm.conf /etc/bluetooth/rfcomm.conf

 And let rfcomm program be used by non-sudo users:

    $ sudo chmod u+s /usr/bin/rfcomm

It is worth to save your time with adding your user to dialout group:

    $ sudo usermod -a -G dialout your_user

Last, you can add to user's .bashrc file following aliasses:

    alias amigobot1_init='rfcomm bind rfcomm1 98:D3:34:90:62:DF'
    alias amigobot2_init='rfcomm bind rfcomm2 98:D3:34:90:63:27'
    alias amigobot1_base_launch='roslaunch amigobot_driver amigobot_driver.launch robot_name:=amigobot1 robot_port:=/dev/rfcomm1 joy:=true joy_control:=true --screen'
    alias amigobot2_base_launch='roslaunch amigobot_driver amigobot_driver.launch robot_name:=amigobot2 robot_port:=/dev/rfcomm2 joy:=true joy_control:=true --screen'
    alias amigobot1_deinit='rfcomm release rfcomm1'
    alias amigobot2_deinit='rfcomm release rfcomm2'

# Usage

Suppose you want to connect to the Pioneer1 robot:

    $ amigobot1_init
    $ amigobot1_base_launch

And in the end you can clean up with:

    $ amigobot1_deinit